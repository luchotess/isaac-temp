/**
 * Created by roman.kupin on 8/14/2017.
 */
import {Pipe, PipeTransform} from "@angular/core";
import {IPersonName} from "../app.model";


@Pipe({name: "personName"})
export class PersonNamePipe implements PipeTransform {

    static Formats = [
        {
            regexKey: /(%f)(?:\:)?(\d*)?/i,
            valueProvider: (personName: IPersonName, match: any[]) => {
                if (!personName.First) return "";
                let hasMaxLength = typeof match[2] !== "undefined";
                return hasMaxLength ? personName.First.substring(0, parseInt(match[2], 10)) : personName.First;
            }
        },
        {
            regexKey: /(%l)(?:\:)?(\d*)?/i,
            valueProvider: (personName: IPersonName, match: any[]) => {
                if (!personName.Last) return "";
                let hasMaxLength = typeof match[2] !== "undefined";
                return hasMaxLength ? personName.Last.substring(0, parseInt(match[2], 10)) : personName.Last;
            }
        },
        {
            regexKey: /(%li)/i,
            valueProvider: (personName: IPersonName, match: any[]) => {
                if (!personName.LastInitial) return "";
                return personName.LastInitial + ".";
            }
        }
    ];

    transform(value: IPersonName, format: string = "%f %l"): string | null {
        if (!value) return null;
        let nameTemplate = format;
        PersonNamePipe.Formats.forEach(definedFormat => {
            nameTemplate = nameTemplate.replace(definedFormat.regexKey, (...args: any[]) => {
                return definedFormat.valueProvider(value, args)
            });
        });

        return nameTemplate;
    }

}