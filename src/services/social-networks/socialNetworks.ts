/**
 * Created by roman.kupin on 8/15/2017.
 */


export class SocialNetwork {
    constructor(public code: string,
                public name: string) {
    }
}

export interface ShareWindowFeatures {
    width: number;
    height: number;
}


export const FACEBOOK = new SocialNetwork("fb", "facebook");
export const TWITTER = new SocialNetwork("tw", "twitter");
export const LINKEDIN = new SocialNetwork("li", "linkedin");

export const SOCIAL_NETWORKS: SocialNetwork[] = [
    FACEBOOK, TWITTER, LINKEDIN
];

export const COMMON_SHARE_WINDOW_FEATURES = "titlebar=0,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=yes,modal=yes,";

export const SHARE_WINDOW_FEATURES: { network: SocialNetwork, features: ShareWindowFeatures }[] = [
    {network: FACEBOOK, features: {width: 640, height: 480}},
    {network: TWITTER, features: {width: 510, height: 500}},
    {network: LINKEDIN, features: {width: 600, height: 440}}
];