/**
 * Created by roman.kupin on 8/16/2017.
 */


import {Action} from "@ngrx/store";

export const SHARED_SUCCESSFULLY = "[Model].ShareSuccess";


export class SharedSuccessfully implements Action {
    readonly type: string = SHARED_SUCCESSFULLY;

    constructor(public data: any,
                public network: string,
                public isFirstShare: boolean) {
    }
}

export type All = SharedSuccessfully;