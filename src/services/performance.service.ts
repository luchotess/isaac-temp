﻿/**
 * Created by Kevin Tiller on 8/23/2017.
 */

import {Store} from "@ngrx/store";
import {AppState} from "../app.state";
import {Jsonp, URLSearchParams} from "@angular/http";
import {TimingService} from "./timing.service";

export class PerformanceService {
    serviceDomain: string;
    companyCampaignCode: string;
    campaignPromoterUUID: string;
    flipToPerformance: any;
    scriptParams: any;
    sent: boolean;

    constructor(private jsonp: Jsonp, private store: Store<AppState>) {
        this.flipToPerformance = (<any>window)['flipToPerformance'] || {};
        this.store.select(state => state.model)
            .subscribe(model => {
                this.companyCampaignCode = model.CompanyCampaignCode;
                this.campaignPromoterUUID = model.Promoter.CampaignPromoterUUID;
                this.serviceDomain = model.ServiceDomain;
                this.scriptParams = model.ScriptParams || {};
                this.sent = !!this.scriptParams.disablePerformance
            });
    }

    // Add a performance event with an optional time that defaults to current timestamp
    public addPerformanceEvent(event: string, time?: number) {
        this.flipToPerformance[event] = time || TimingService.getTimestamp();
    }


    // Send the final performance data to the server (happens at MOST once)
    public sendPerformanceData() {
        // Don't send more than once
        if (this.sent) {
            return;
        }
        if (performance && performance.timing) {
            this.flipToPerformance['connectEnd'] = performance.timing.connectEnd;
            this.flipToPerformance['connectStart'] = performance.timing.connectStart;
            this.flipToPerformance['domComplete'] = performance.timing.domComplete;
            this.flipToPerformance['domContentLoadedEventEnd'] = performance.timing.domContentLoadedEventEnd;
            this.flipToPerformance['domContentLoadedEventStart'] = performance.timing.domContentLoadedEventStart;
            this.flipToPerformance['domInteractive'] = performance.timing.domInteractive;
            this.flipToPerformance['domLoading'] = performance.timing.domLoading;
            this.flipToPerformance['domainLookupEnd'] = performance.timing.domainLookupEnd;
            this.flipToPerformance['domainLookupStart'] = performance.timing.domainLookupStart;
            this.flipToPerformance['fetchStart'] = performance.timing.fetchStart;
            this.flipToPerformance['loadEventEnd'] = performance.timing.loadEventEnd;
            this.flipToPerformance['loadEventStart'] = performance.timing.loadEventStart;
            this.flipToPerformance['navigationStart'] = performance.timing.navigationStart;
            this.flipToPerformance['redirectEnd'] = performance.timing.redirectEnd;
            this.flipToPerformance['redirectStart'] = performance.timing.redirectStart;
            this.flipToPerformance['requestStart'] = performance.timing.requestStart;
            this.flipToPerformance['responseEnd'] = performance.timing.responseEnd;
            this.flipToPerformance['responseStart'] = performance.timing.responseStart;
            this.flipToPerformance['secureConnectionStart'] = performance.timing.secureConnectionStart;
            this.flipToPerformance['unloadEventEnd'] = performance.timing.unloadEventEnd;
            this.flipToPerformance['unloadEventStart'] = performance.timing.unloadEventStart;
        }

        this.flipToPerformance['companyCampaignCode'] = this.companyCampaignCode;
        this.flipToPerformance['screenWidth'] = document.documentElement.clientWidth;
        this.flipToPerformance['displayType'] = 'ConfirmationX';
        this.flipToPerformance['url'] = window.location.href;
        this.flipToPerformance['campaign_promoter_uuid'] = this.campaignPromoterUUID;
        this.flipToPerformance['apiVersion'] = this.flipToPerformance.apiVersion || this.scriptParams.ver || 0;

        // Send it as a JSONP request to avoid cross-domain problems
        let params = new URLSearchParams();
        for (let key in this.flipToPerformance) {
            params.set(key, this.flipToPerformance[key])
        }
        let url = document.location.protocol + '//' + this.serviceDomain + '/confirmation/performance?' + params.toString();
        this.jsonp.request(url).subscribe(
            // Successful responses call the first callback.
            data => {
            },
            // Errors will call this callback instead:
            err => {
            }
        );
        this.sent = true;
    }
}
