/**
 * Created by roman.kupin on 8/9/2017.
 */


import {Component, Injectable} from "@angular/core";

@Component({
    selector: "ft-overlay",
    styles: [`
        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: block;
            overflow: auto;

            color: blue;
            background: #ccc;
        }
    `],
    template: `
        <div class="overlay">
            <ng-content #content></ng-content>
        </div>
    `
})
export class OverlayComponent {
}


@Injectable()
export class OverlayService {
}