/**
 * Created by roman.kupin on 8/8/2017.
 */

import {Action} from "@ngrx/store";

export const NO_VIEW = "[View].NothingToShow";
export const INTRO_VIEW = "[View].Intro";
export const MAIN_VIEW = "[View].Main";
export const MAIN_VIEW_CONTINUE = "[View].Main.Continue";
export const REOPENER_VIEW = "[View].Reopener";


export class NoView implements Action {
    readonly type: string = NO_VIEW;
}

export class Intro implements Action {
    readonly type: string = INTRO_VIEW;
}

export class Main implements Action {
    readonly type: string = MAIN_VIEW;
}

export class MainViewContinue implements Action {
    readonly type: string = MAIN_VIEW_CONTINUE;
}

export class Reopener implements Action {
    readonly type: string = REOPENER_VIEW;
}

export type All = NoView | Intro | Main | Reopener | MainViewContinue;