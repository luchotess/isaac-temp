﻿/**
 * Created by Kevin Tiller on 8/23/2017.
 */
export class TimingService {
    // TODO: THERE BE DRAGONS SIMILAR CODE EXISTS IN confirmation.aspx and buzz.js
    // Gets a timestamp as precisely as the browser supports
    public static getTimestamp() {
        try {
            return !!performance && !!performance.timing && !!performance.timing.navigationStart && !!performance.now && typeof performance.now === "function"
                ? (performance.timing.navigationStart + performance.now())
                : new Date().getTime();
        } catch (e) {
            return new Date().getTime();
        }
    }

}