/**
 * Created by roman.kupin on 8/14/2017.
 */
import { Subject } from "rxjs/Subject";
import { TrackingService } from "../../services/tracking.service";


export class Panel {
    constructor(public id: string,
                public componentRef: any) {
    }
}

export class PanelsService {
  constructor(
    private trackingService: TrackingService) {
  }

    activePanel: Subject<Panel> = new Subject<Panel>();

    panels: Panel[] = [];

    addPanel(panel: Panel) {
        this.panels.push(panel);
    }

    setActive(panel: Panel) {
        this.activePanel.next(panel);
    }

    setActiveById(panelId: string) {
        this.trackingService.addTrackingEvent(panelId + "View");
        let panel = this.panels.find(value => value.id === panelId);
        if (!!panel) {
            this.setActive(panel);
        }
    }

}