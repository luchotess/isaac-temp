/**
 * Created by roman.kupin on 8/16/2017.
 */

import {MainViewUI, UIState} from "../app.state";
import {Action} from "@ngrx/store";
import {ViewActions, ModelActions} from "../actions/index";
import {SharedSuccessfully} from "../actions/model.actions";


export function uiReducer(state: UIState, action: Action): UIState {

    switch (action.type) {
        case ViewActions.MAIN_VIEW: {
            state.MainView = new MainViewUI();
            state.MainView.screen = MainViewUI.SHARE_SCREEN;
            return state;
        }
        case ModelActions.SHARED_SUCCESSFULLY: {
            if ((<SharedSuccessfully>action).isFirstShare) {
                state.MainView.screen = MainViewUI.SHARED_SCREEN;
            }
            return state;
        }
        case ViewActions.MAIN_VIEW_CONTINUE: {
            state.MainView.screen = MainViewUI.SHARE_SCREEN;
            return state;
        }
        default: {
            return state;
        }
    }

}