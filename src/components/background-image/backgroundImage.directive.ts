/**
 * Created by roman.kupin on 8/14/2017.
 */
import {Directive, ElementRef, Input, OnDestroy, Renderer2} from "@angular/core";
import {RemoteImageService} from "../../services/remoteImage.service";
import {Subscription} from "rxjs/Subscription";


@Directive({
    selector: "[ftBackgroundImage]"
})
export class BackgroundImageDirective implements OnDestroy {

    private remoteImageSub: Subscription;

    constructor(public elemRef: ElementRef,
                public renderer: Renderer2,
                public remoteImage: RemoteImageService) {
    }

    @Input() fallbackImage: string | null;
    @Input() fallbackClass: string | null;

    @Input()
    set ftBackgroundImage(value: string) {
        this.remoteImageSub = this.remoteImage.checkExistence(value)
            .subscribe(exists => {
                if (exists) {
                    this.setBackgroundUrl(value);
                } else if (!!this.fallbackImage) {
                    this.setBackgroundUrl(this.fallbackImage);
                } else if (!!this.fallbackClass) {
                    this.fallbackClass.split(" ").forEach(klass => this.addClass(klass));
                }
            });
    }

    private setBackgroundUrl(url: string) {
        this.renderer.setStyle(this.elemRef.nativeElement, "background-image", `url('${url}')`);
    }

    private addClass(klass: string) {
        this.renderer.addClass(this.elemRef.nativeElement, klass);
    }

    ngOnDestroy(): void {
        this.remoteImageSub.unsubscribe();
    }

}
