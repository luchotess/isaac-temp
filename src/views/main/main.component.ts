/**
 * Created by roman.kupin on 8/8/2017.
 */

import {Component, OnDestroy, OnInit, ViewEncapsulation, AfterViewInit} from "@angular/core";
import {AppState, MainViewUI} from "../../app.state";
import {Store} from "@ngrx/store";
import {Property, Promotion, Photo, Promoter} from "../../app.model";
import {LoopbackService, Signal} from "../../services/signalrLoopback.service";
import {Observable} from "rxjs/Observable";
import {SocialNetworkShareWindowService} from "../../services/social-networks/shareWindow.service";
import {FACEBOOK, LINKEDIN, SocialNetwork, TWITTER} from "../../services/social-networks/socialNetworks";
import { ViewActions, ModelActions } from "../../actions/index";
import { TrackingService } from "../../services/tracking.service";


@Component({
    selector: "ft-main",
    templateUrl: "./main.component.html",
    styleUrls: ["./main.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {
    screen: Store<string>;
    liPromotions: Store<Promotion[]>;
    twPromotions: Store<Promotion[]>;
    fbPromotions: Store<Promotion[]>;

    property: Property;
    shareMessage: string;
    incentiveText: string;
    promotionCode: string;

    Facebook: SocialNetwork = FACEBOOK;
    Twitter: SocialNetwork = TWITTER;
    Linkedin: SocialNetwork = LINKEDIN;

    SHARE_SCREEN = MainViewUI.SHARE_SCREEN;
    SHARED_SCREEN = MainViewUI.SHARED_SCREEN;
    hostedLandingPageHasGallery: boolean;
    guestPhotos: Photo[];
    promoter: Promoter;

    constructor(private store: Store<AppState>,
                private loopbackService: LoopbackService,
                private shareWindowService: SocialNetworkShareWindowService,
                private trackingService: TrackingService) {

        this.store.select(state => state.model)
            .subscribe(model => {
                this.promotionCode = model.Promoter.PromotionCode;
                this.incentiveText = model.Offer[0].FormalText;
                this.shareMessage = model.Share.PostMessage;
                this.property = model.Property;
                this.hostedLandingPageHasGallery = model.GuestPhotos.length >= 5;
                this.guestPhotos = model.GuestPhotos;
                this.promoter = model.Promoter;
            });

        this.fbPromotions = this.store.select(state => state.model.Promoter.Promotions.filter(p => p.Title.toLowerCase() === FACEBOOK.code));
        this.twPromotions = this.store.select(state => state.model.Promoter.Promotions.filter(p => p.Title.toLowerCase() === TWITTER.code));
        this.liPromotions = this.store.select(state => state.model.Promoter.Promotions.filter(p => p.Title.toLowerCase() === LINKEDIN.code));

        this.screen = this.store.select(state => state.UI.MainView.screen);

        this.loopbackService.Signal.withLatestFrom(this.store.select(state => state.model.Promoter.Promotions))
            .subscribe(([signal, promotions]) => {
                if (signal["event"] === "share-complete") {
                    if (!!signal["success"]) {
                        this.store.dispatch(new ModelActions.SharedSuccessfully(signal["data"], signal["network"], promotions.length === 0));
                    }
                }
            });
    }

    ngOnInit(): void {
        this.loopbackService.start();
    }

    ngAfterViewInit() {
        let dataLayer: any = (<any>window)['dataLayer'] || {};
        dataLayer.push({ 'event': 'flipto.optimizeActivate' });
    }

    ngOnDestroy(): void {
        this.loopbackService.stop();
    }

    onClose() {
        this.store.dispatch(new ViewActions.Reopener());
    }

    onContinue() {
        this.store.dispatch(new ViewActions.MainViewContinue());
    }

    shareTo(network: SocialNetwork) {

        if (network == FACEBOOK) {
            this.trackingService.overrideTrackingEvent("shareOnFacebookClick");
        } else if (network == TWITTER) {
            this.trackingService.overrideTrackingEvent("shareOnTwitterClick");
        } else if (network == LINKEDIN) {
            this.trackingService.overrideTrackingEvent("shareOnLinkedinClick");
        }

        this.getShareUrl(network)
            .subscribe(url => {
                this.shareWindowService.open(network, url);
            });
    }

    private getShareUrl(network: SocialNetwork): Observable<string> {

        return this.store.select(state => {
            let CompanyCampaignCode, Property, Promoter, Language, Share, ServiceDomain;
            ({CompanyCampaignCode, Property, Promoter, Language, Share, ServiceDomain} = state.model);

            let postLink = `http://${ServiceDomain}/${Promoter.PromotionCode}-${network.code}`;
            let queryStringData = {
                "cid": CompanyCampaignCode,
                "logo": Property.Logo,
                "link": postLink,
                "flipto": Promoter.PromotionCode,
                "lang": Language.Code,
                "isusercomment": true, //Always true, FB requires it
                "email": `${null}`,
                "status": `${Share.PostMessage} ${postLink}`,
                "signame": Property.DisplayName,
                "desc": Property.Description,
                "picture": Property.PostThumbnailImage,
                "connectionId": this.loopbackService.getConnectionId()
            };

            const queryString = Object.keys(queryStringData).map(key => `${key}=${encodeURIComponent((<any>queryStringData)[key])}`).reduce((previousValue, currentValue) => `${previousValue}&${currentValue}`, "").substring(1);

            return `http://${ServiceDomain}/oauth/${network.name}/auth.aspx?${queryString}`;

        }).first();
    };

}