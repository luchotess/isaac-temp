/**
 * Created by roman.kupin on 8/7/2017.
 */

import * as ViewActions from "../actions/view.actions";

export type Action = ViewActions.All;

export function viewReducer(state: string, action: Action): string {
    switch (action.type) {
        case ViewActions.INTRO_VIEW:
        case ViewActions.MAIN_VIEW:
        case ViewActions.NO_VIEW:
        case ViewActions.REOPENER_VIEW:{
            state = action.type;
            return state;
        }
        default: {
            return state;
        }
    }
}