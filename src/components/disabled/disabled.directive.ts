/**
 * Created by roman.kupin on 8/16/2017.
 */
import {Directive, ElementRef, Input, Renderer2} from "@angular/core";

@Directive({
    selector: "[ftDisabled]"
})
export class DisabledDirective {

    @Input("disableElement") isDisableElement: boolean = true;

    constructor(public elemRef: ElementRef,
                public renderer: Renderer2) {
    }

    @Input()
    set ftDisabled(isDisabled: boolean) {
        this.toggleElement(isDisabled);
        this.adjustCss(isDisabled);
    }

    toggleElement(isDisabled: boolean) {
        if (this.isDisableElement) {
            this.renderer.setStyle(this.elemRef.nativeElement, "pointer-events", `${isDisabled ? 'none' : ''}`);
        }
    }

    adjustCss(isDisabled: boolean) {
        if (isDisabled) {
            this.renderer.addClass(this.elemRef.nativeElement, "disabled");
        } else {
            this.renderer.removeClass(this.elemRef.nativeElement, "disabled");
        }
    }

}