/**
 * Created by roman.kupin on 8/14/2017.
 */
import {Component} from "@angular/core";
import {PanelsService} from "./panels.service";


@Component({
    selector: "ft-panels",
    template: `
        <ng-content></ng-content>
    `,
    providers: [PanelsService]
})
export class PanelsComponent {

    constructor(public panels: PanelsService) {
    }

}
