/**
 * Created by roman.kupin on 8/14/2017.
 */
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: "guestShareUrl"})
export class GuestShareUrlPipe implements PipeTransform {
    transform(value: string): string | null {
        if (!value) return null;
        return `http://flip.to/${value}`;
    }

}