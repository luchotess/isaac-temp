/**
 * Created by roman.kupin on 8/23/2017.
 */

import {Inject, Injectable, Renderer2} from "@angular/core";
import {DOCUMENT} from "@angular/common";
import {AppState} from "../app.state";
import {Store} from "@ngrx/store";


@Injectable()
export class PageScrollLockService {

    htmlElement: any;

    constructor(@Inject(DOCUMENT) private document: any,
                public renderer: Renderer2,
                private store: Store<AppState>) {
        this.htmlElement = document.querySelector("html");
    }

    lockOnViewChange(views: string[]) {
        this.store.select(state => state.view)
            .subscribe(view => {
                if (!!views.filter(value => value === view).length) {
                    this.lock();
                } else {
                    this.release();
                }
            });
    }

    lock() {
        this.renderer.addClass(this.htmlElement, "ft-app-open");
    }

    release() {
        this.renderer.removeClass(this.htmlElement, "ft-app-open");
    }

}