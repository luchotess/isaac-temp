/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as i0 from './reopener.component.css.ngstyle';
import * as i1 from '@angular/core';
import * as i2 from '../../components/background-image/backgroundImage.directive';
import * as i3 from '../../services/remoteImage.service';
import * as i4 from '../../components/guest-photos/guestPhotos.component.ngfactory';
import * as i5 from '../../components/guest-photos/guestPhotos.component';
import * as i6 from '@ngrx/store';
import * as i7 from './reopener.component';
import * as i8 from '@angular/common';
import * as i9 from '../../services/tracking.service';
const styles_ReopenerComponent:any[] = [i0.styles];
export const RenderType_ReopenerComponent:i1.RendererType2 = i1.ɵcrt({encapsulation:2,
    styles:styles_ReopenerComponent,data:{}});
function View_ReopenerComponent_1(_l:any):i1.ɵViewDefinition {
  return i1.ɵvid(0,[(_l()(),i1.ɵeld(0,0,(null as any),(null as any),1,'div',[['class',
      'ft-profile-pic']],(null as any),(null as any),(null as any),(null as any),(null as any))),
      i1.ɵdid(1,147456,(null as any),0,i2.BackgroundImageDirective,[i1.ElementRef,
          i1.Renderer2,i3.RemoteImageService],{ftBackgroundImage:[0,'ftBackgroundImage']},
          (null as any))],(_ck,_v) => {
    var _co:any = _v.component;
    const currVal_0:any = ((_co.property == null)? (null as any): ((_co.property.GeneralManager == null)? (null as any): _co.property.GeneralManager.ProfileImage));
    _ck(_v,1,0,currVal_0);
  },(null as any));
}
export function View_ReopenerComponent_0(_l:any):i1.ɵViewDefinition {
  return i1.ɵvid(0,[(_l()(),i1.ɵeld(0,0,(null as any),(null as any),25,'div',[['class',
      'ft-confirmation-component ft-confirmation-reopener']],(null as any),(null as any),
      (null as any),(null as any),(null as any))),(_l()(),i1.ɵted(-1,(null as any),
      ['\n    '])),(_l()(),i1.ɵeld(2,0,(null as any),(null as any),4,'div',[['class',
      'ft-reopener-image']],(null as any),(null as any),(null as any),(null as any),
      (null as any))),(_l()(),i1.ɵted(-1,(null as any),['\n        '])),(_l()(),i1.ɵeld(4,
      0,(null as any),(null as any),1,'ft-guest-photos',([] as any[]),(null as any),
      (null as any),(null as any),i4.View_GuestPhotosComponent_0,i4.RenderType_GuestPhotosComponent)),
      i1.ɵdid(5,114688,(null as any),0,i5.GuestPhotosComponent,[i6.Store],{maxGuestPhotos:[0,
          'maxGuestPhotos']},(null as any)),(_l()(),i1.ɵted(-1,(null as any),['\n    '])),
      (_l()(),i1.ɵted(-1,(null as any),['\n    '])),(_l()(),i1.ɵeld(8,0,(null as any),
          (null as any),4,'div',[['class','ft-reopener-close']],(null as any),[[(null as any),
              'click']],(_v,en,$event) => {
            var ad:boolean = true;
            var _co:i7.ReopenerComponent = _v.component;
            if (('click' === en)) {
              const pd_0:any = ((<any>_co.hide()) !== false);
              ad = (pd_0 && ad);
            }
            return ad;
          },(null as any),(null as any))),(_l()(),i1.ɵted(-1,(null as any),['\n        '])),
      (_l()(),i1.ɵeld(10,0,(null as any),(null as any),1,'div',[['class','ft-reopener-close-text']],
          (null as any),(null as any),(null as any),(null as any),(null as any))),
      (_l()(),i1.ɵted(-1,(null as any),['Hide'])),(_l()(),i1.ɵted(-1,(null as any),
          ['\n    '])),(_l()(),i1.ɵted(-1,(null as any),['\n    '])),(_l()(),i1.ɵeld(14,
          0,(null as any),(null as any),10,'div',[['class','ft-reopener-content']],
          (null as any),[[(null as any),'click']],(_v,en,$event) => {
            var ad:boolean = true;
            var _co:i7.ReopenerComponent = _v.component;
            if (('click' === en)) {
              const pd_0:any = ((<any>_co.reopen()) !== false);
              ad = (pd_0 && ad);
            }
            return ad;
          },(null as any),(null as any))),(_l()(),i1.ɵted(-1,(null as any),['\n        '])),
      (_l()(),i1.ɵand(16777216,(null as any),(null as any),1,(null as any),View_ReopenerComponent_1)),
      i1.ɵdid(17,16384,(null as any),0,i8.NgIf,[i1.ViewContainerRef,i1.TemplateRef],
          {ngIf:[0,'ngIf']},(null as any)),(_l()(),i1.ɵted(-1,(null as any),['\n        '])),
      (_l()(),i1.ɵeld(19,0,(null as any),(null as any),4,'div',[['class','ft-reopener-message']],
          (null as any),(null as any),(null as any),(null as any),(null as any))),
      (_l()(),i1.ɵted(-1,(null as any),['\n            Share the excitement and receive a bonus.\n            '])),
      (_l()(),i1.ɵeld(21,0,(null as any),(null as any),1,'a',([] as any[]),(null as any),
          (null as any),(null as any),(null as any),(null as any))),(_l()(),i1.ɵted(-1,
          (null as any),['Take a look ➞'])),(_l()(),i1.ɵted(-1,(null as any),['\n        '])),
      (_l()(),i1.ɵted(-1,(null as any),['\n    '])),(_l()(),i1.ɵted(-1,(null as any),
          ['\n']))],(_ck,_v) => {
    var _co:i7.ReopenerComponent = _v.component;
    const currVal_0:any = 5;
    _ck(_v,5,0,currVal_0);
    const currVal_1:boolean = !!((_co.property == null)? (null as any): ((_co.property.GeneralManager == null)? (null as any): _co.property.GeneralManager.ProfileImage));
    _ck(_v,17,0,currVal_1);
  },(null as any));
}
export function View_ReopenerComponent_Host_0(_l:any):i1.ɵViewDefinition {
  return i1.ɵvid(0,[(_l()(),i1.ɵeld(0,0,(null as any),(null as any),1,'ft-reopener',
      ([] as any[]),(null as any),(null as any),(null as any),View_ReopenerComponent_0,
      RenderType_ReopenerComponent)),i1.ɵdid(1,4243456,(null as any),0,i7.ReopenerComponent,
      [i6.Store,i9.TrackingService],(null as any),(null as any))],(null as any),(null as any));
}
export const ReopenerComponentNgFactory:i1.ComponentFactory<i7.ReopenerComponent> = i1.ɵccf('ft-reopener',
    i7.ReopenerComponent,View_ReopenerComponent_Host_0,{},{},([] as any[]));
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiQzovR2l0L1BsYXRmb3JtL0ZsaXBUb1NpdGUvZGlzdC1hcHBzL2NvbmZpcm1hdGlvbi94L2NsaWVudC9zcmMvdmlld3MvcmVvcGVuZXIvcmVvcGVuZXIuY29tcG9uZW50Lm5nZmFjdG9yeS50cyIsInZlcnNpb24iOjMsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5nOi8vL0M6L0dpdC9QbGF0Zm9ybS9GbGlwVG9TaXRlL2Rpc3QtYXBwcy9jb25maXJtYXRpb24veC9jbGllbnQvc3JjL3ZpZXdzL3Jlb3BlbmVyL3Jlb3BlbmVyLmNvbXBvbmVudC50cyIsIm5nOi8vL0M6L0dpdC9QbGF0Zm9ybS9GbGlwVG9TaXRlL2Rpc3QtYXBwcy9jb25maXJtYXRpb24veC9jbGllbnQvc3JjL3ZpZXdzL3Jlb3BlbmVyL3Jlb3BlbmVyLmNvbXBvbmVudC5odG1sIiwibmc6Ly8vQzovR2l0L1BsYXRmb3JtL0ZsaXBUb1NpdGUvZGlzdC1hcHBzL2NvbmZpcm1hdGlvbi94L2NsaWVudC9zcmMvdmlld3MvcmVvcGVuZXIvcmVvcGVuZXIuY29tcG9uZW50LnRzLlJlb3BlbmVyQ29tcG9uZW50X0hvc3QuaHRtbCJdLCJzb3VyY2VzQ29udGVudCI6WyIgIiwiPGRpdiBjbGFzcz1cImZ0LWNvbmZpcm1hdGlvbi1jb21wb25lbnQgZnQtY29uZmlybWF0aW9uLXJlb3BlbmVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiZnQtcmVvcGVuZXItaW1hZ2VcIj5cclxuICAgICAgICA8ZnQtZ3Vlc3QtcGhvdG9zIFttYXhHdWVzdFBob3Rvc109XCI1XCI+PC9mdC1ndWVzdC1waG90b3M+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJmdC1yZW9wZW5lci1jbG9zZVwiIChjbGljayk9XCJoaWRlKClcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiZnQtcmVvcGVuZXItY2xvc2UtdGV4dFwiPkhpZGU8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cImZ0LXJlb3BlbmVyLWNvbnRlbnRcIiAoY2xpY2spPVwicmVvcGVuKClcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiZnQtcHJvZmlsZS1waWNcIlxyXG4gICAgICAgICAgICAgKm5nSWY9XCIhIXByb3BlcnR5Py5HZW5lcmFsTWFuYWdlcj8uUHJvZmlsZUltYWdlXCJcclxuICAgICAgICAgICAgIFtmdEJhY2tncm91bmRJbWFnZV09XCJwcm9wZXJ0eT8uR2VuZXJhbE1hbmFnZXI/LlByb2ZpbGVJbWFnZVwiPjwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJmdC1yZW9wZW5lci1tZXNzYWdlXCIgaTE4bj1cInh4eHx5eXlAcmVvcGVuZXIuenp6XCI+XHJcbiAgICAgICAgICAgIFNoYXJlIHRoZSBleGNpdGVtZW50IGFuZCByZWNlaXZlIGEgYm9udXMuXHJcbiAgICAgICAgICAgIDxhPlRha2UgYSBsb29rIOKenjwvYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj4iLCI8ZnQtcmVvcGVuZXI+PC9mdC1yZW9wZW5lcj4iXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O29CQ1FRO01BQUE7YUFBQTs0Q0FBQTtVQUFBOztJQUVLO0lBRkwsV0FFSyxTQUZMOzs7O29CQVJSO01BQUE7TUFBQSw0Q0FBZ0U7TUFBQSxhQUM1RDtNQUFBO01BQUEsZ0JBQStCLGtEQUMzQjtNQUFBO01BQUE7YUFBQTtVQUFBLGtDQUF3RDtNQUN0RCw4Q0FDTjtVQUFBO2NBQUE7WUFBQTtZQUFBO1lBQStCO2NBQUE7Y0FBQTtZQUFBO1lBQS9CO1VBQUEsZ0NBQWdEO01BQzVDO1VBQUE7TUFBb0MsNENBQVU7VUFBQSxhQUM1Qyw4Q0FDTjtVQUFBO1VBQUE7WUFBQTtZQUFBO1lBQWlDO2NBQUE7Y0FBQTtZQUFBO1lBQWpDO1VBQUEsZ0NBQW9EO01BQ2hEO2FBQUE7VUFBQSxpQ0FFd0U7TUFDeEU7VUFBQTtNQVhSO01BRVk7VUFBQSwwREFBSTtVQUFBLGtDQUFpQjtNQVluQiw4Q0FDSjtVQUFBOztJQWJlO0lBQWpCLFdBQWlCLFNBQWpCO0lBT0s7SUFETCxZQUNLLFNBREw7Ozs7b0JDUlI7TUFBQTtrQ0FBQSxVQUFBO01BQUE7OzsifQ==
