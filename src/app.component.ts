/**
 * Created by roman.kupin on 8/3/2017.
 */


import {
    Component, OnInit, Type, ViewEncapsulation
} from "@angular/core";
import {IntroComponent} from "./views/intro/intro.component";
import {Store} from "@ngrx/store";
import {AppState} from "./app.state";
import * as ViewActions from "./actions/view.actions";
import {MainComponent} from "./views/main/main.component";
import {ReopenerComponent} from "./views/reopener/reopener.component";
import {PageScrollLockService} from "./services/pageScrollLock.service";
import {INTRO_VIEW, MAIN_VIEW, MAIN_VIEW_CONTINUE} from "./actions/view.actions";

@Component({
    selector: "ft-confirmation-app",
    template: `
        <div class="ft-confirmation-app">
            <ng-container *ngComponentOutlet="ViewComponent"></ng-container>
        </div>
    `,
    styleUrls: ["./app.component.css"],
    encapsulation: ViewEncapsulation.None,
    providers: [PageScrollLockService]
})
export class AppComponent implements OnInit {
    ViewComponent: Type<any> | null;
    viewObservable: Store<string>;

    constructor(private store: Store<AppState>,
                private pageScrollLockService: PageScrollLockService) {
        this.viewObservable = this.store.select("view");
        this.pageScrollLockService.lockOnViewChange([INTRO_VIEW, MAIN_VIEW, MAIN_VIEW_CONTINUE]);
    }

    ngOnInit() {
        this.viewObservable.subscribe((view) => {
            this.ViewComponent = AppComponent.resolveComponent(view);
        });
    }

    static resolveComponent(view: string) {
        switch (view) {
            case ViewActions.INTRO_VIEW: {
                return IntroComponent;
            }
            case ViewActions.MAIN_VIEW: {
                return MainComponent;
            }
            case ViewActions.REOPENER_VIEW: {
                return ReopenerComponent;
            }
            case ViewActions.NO_VIEW: {
                return null;
            }
            default: {
                throw `${view} view is not supported.`;
            }
        }
    }

    ngOnDestroy() {
    }

}
