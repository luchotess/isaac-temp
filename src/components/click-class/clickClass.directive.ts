/**
 * Created by roman.kupin on 8/23/2017.
 */

import {Directive, ElementRef, HostListener, Input, Renderer2} from "@angular/core";


@Directive({
    selector: "[ftClickClass]"
})
export class ClickClassDirective {

    private openedAt: number;
    @Input("ftClickClass") clickClass: string;

    constructor(public elRef: ElementRef,
                public renderer: Renderer2) {
    }


    @HostListener("click")
    open() {
        this.openedAt = +(Date.now());
        this.renderer.addClass(this.elRef.nativeElement, this.clickClass);
    }

    @HostListener("document:click")
    close() {
        if (Date.now() - this.openedAt > 50) {
            this.renderer.removeClass(this.elRef.nativeElement, this.clickClass);
        }
    }

}