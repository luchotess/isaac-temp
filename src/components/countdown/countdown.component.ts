/**
 * Created by roman.kupin on 8/17/2017.
 */
import {
    Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild,
    ViewEncapsulation
} from "@angular/core";


@Component({
    selector: "ft-countdown",
    template: `
        <span class="ft-countdown-timer">
            <svg class="timer" viewBox="0 0 34 34">
                <g>
                    <circle #timerCircle class="timerCircle" cx="15" cy="17" r="15.9" stroke-width="4"
                            transform="rotate(-90 16 16)"></circle>
                    <text #timerText class="timerText" x="17" y="24.5"></text>
                    <line class="timerVertLine" *ngIf="canPause" x1="13.5" x2="13.5" y1="10" y2="24"
                          stroke-width="3.5"/>
                    <line class="timerVertLine" *ngIf="canPause" x1="20.5" x2="20.5" y1="10" y2="24"
                          stroke-width="3.5"/>
                </g>
            </svg>
        <span>
    `,
    styleUrls: ["./countdown.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class CountdownComponent implements OnInit {

    @ViewChild("timerText", {read: ElementRef}) timerText: ElementRef;
    @ViewChild("timerCircle", {read: ElementRef}) timerCircle: ElementRef;

    @Input() timeoutSeconds: number;

    @Input() canPause: boolean = false;

    @Output() onTimeout = new EventEmitter();
    @Output() onPause = new EventEmitter();
    private isPaused: boolean = false;

    ngOnInit(): void {
        let totalTime: number = this.timeoutSeconds,
            currentTime: number = totalTime,
            percentTime = null;

        this.updateCountdownText(currentTime);

        let timer = () => {
            currentTime -= 1;
            if (this.canPause && this.isPaused) {
                return;
            }
            if (currentTime === -1) {
                this.onTimeout.emit();
                return;
            }
            this.updateCountdownText(currentTime);
            percentTime = Math.round((currentTime / totalTime) * 100);
            this.timerCircle.nativeElement.style["strokeDashoffset"] = percentTime - 100;

            setTimeout(() => requestAnimationFrame(timer), 1000);
        };

        timer();
    }

    updateCountdownText(timeLeft: number) {
        if (!this.canPause) {
            this.timerText.nativeElement.textContent = timeLeft.toString();
        }
    }

    @HostListener("click")
    onTimerClick() {
        if (this.canPause) {
            this.isPaused = true;
            this.onPause.emit();
        }
    }

}