/**
 * Created by roman.kupin on 8/18/2017.
 */


const path = require('path');
var webpack = require("webpack");

module.exports = {
    entry: {
        "app": ["./app.imports.js"
        ]
    },
    resolve: {
        alias: {
            typekit: "./typekit.js",
            jquery: "./node_modules/jquery/src/jquery.js",
            signalr: "./signalr/jquery.signalR-2.2.2.min.js",
            hubs: "./signalr/hubs.js",
            zonejs: "./node_modules/zone.js/dist/zone.min.js",
            bundle: "./dist/bundle.js"
        }
    },
    module: {
        noParse: /typekit/
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin()
    ],
    devtool: "source-map",
    output: {
        filename: './app.min.js',
        path: path.resolve(__dirname, 'dist')
    }
};