/**
 * Created by roman.kupin on 8/8/2017.
 */

import {Component, ViewEncapsulation, AfterViewInit} from "@angular/core";
import {AppState} from "../../app.state";
import {Store} from "@ngrx/store";
import * as ViewActions from "../../actions/view.actions";
import { Property } from "../../app.model";
import { TrackingService } from "../../services/tracking.service";

@Component({
    selector: "ft-reopener",
    templateUrl: "./reopener.component.html",
    styleUrls: ["./reopener.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class ReopenerComponent implements AfterViewInit{
    property: Property;

    constructor(private store: Store<AppState>,
      private trackingService: TrackingService) {
        this.store.select(state => state.model)
            .subscribe(model => {
                this.property = model.Property;
            });
    }

    ngAfterViewInit() {
        let dataLayer: any = (<any>window)['dataLayer'] || {};
        dataLayer.push({ 'event': 'flipto.optimizeActivate' });
    }

    hide() {
        this.trackingService.addTrackingEvent("closeClick");
        this.store.dispatch(new ViewActions.NoView());
    }

    reopen() {
        this.trackingService.addTrackingEvent("reopenClick");
        this.store.dispatch(new ViewActions.Main());
    }
}