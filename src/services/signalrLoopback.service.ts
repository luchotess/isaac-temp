/**
 * Created by roman.kupin on 8/15/2017.
 */

import {Subject} from "rxjs/Subject";
import {AppState} from "../app.state";
import {Store} from "@ngrx/store";


declare let $: any;


export type Signal = { event: string, data: any, success: boolean, network: string };

export class LoopbackService {

    url: string = "http://flip.to/signalr";

    constructor(public store: Store<AppState>){
        this.store.select(state => state.model)
            .subscribe(model => {
                this.url = model.SignalrUrl;
            });
    }

    start(): void {
        $["connection"]["hub"]["url"] = this.url;
        let proxy = $["connection"]["loopbackHub"];
        proxy["client"]["receive"] = (signal: any) => {
            this.receive(signal);
        };
        $["connection"]["hub"]["start"]();
    }

    stop(): void {
        $["connection"]["hub"]["stop"]();
    }

    Signal = new Subject<Signal>();

    receive(signal: Signal): void {
        this.Signal.next(signal);
    }

    getConnectionId(): string {
        return $["connection"]["hub"]["id"];
    }
}
