/**
 * Created by roman.kupin on 8/14/2017.
 */


import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {Panel, PanelsService} from "./panels.service";
import {Subscription} from "rxjs/Subscription";


@Component({
    selector: "ft-panel",
    template: `
        <ng-content *ngIf="isActive"></ng-content>
    `,
    encapsulation: ViewEncapsulation.None
})
export class PanelComponent implements OnInit, OnDestroy {

    activePanelSubscription: Subscription;
    @Input("id") id: string;
    @Input("active") isActive: boolean = false;

    constructor(public panels: PanelsService) {
        this.activePanelSubscription = this.panels.activePanel.subscribe(activePanel => {
            this.isActive = activePanel.id === this.id;
        })
    }

    ngOnInit(): void {
        this.panels.addPanel(new Panel(this.id, this));
    }

    ngOnDestroy(): void {
        this.activePanelSubscription.unsubscribe();
    }

}
