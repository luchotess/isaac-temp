/**
 * Created by roman.kupin on 8/17/2017.
 */

import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {Store} from "@ngrx/store";
import {AppState} from "../../app.state";
import {HotelPhoto, Photo} from "../../app.model";
import {Observable} from "rxjs/Observable";


@Component({
    selector: "ft-guest-photos",
    exportAs: "guestPhotos",
    template: `
        <div class="ft-photos-list ft-x-{{companyCampaignCode | async}}"
             *ngIf="totalPhotos >= 3"
             [ngClass]="[totalPhotos < 3 ? '' : totalPhotos < 5 ? 'ft-count-3' : totalPhotos < 10 ? 'ft-count-5' : 'ft-count-10']">
            <div *ngFor="let photo of photos | async; index as i;"
                 class="ft-photo ft-photo-{{i + 1}}"
                 [ftBackgroundImage]="photo.PhotoUriLarge">
                <div class="ft-photo-details">
                    <div class="ft-profile-pic" [ftBackgroundImage]="photo.ProfilePhotoUrl"
                         [fallbackClass]="photo.SilhouetteClass"></div>
                    <div class="ft-profile-name">{{photo.FirstName}} {{photo.LastInitial}}.</div>
                    <div class="ft-photo-caption">“{{photo.Quote}}”</div>
                </div>
            </div>
        </div>
        <div class="ft-photos-list ft-x-{{companyCampaignCode | async}} ft-hotel-photo"
             *ngIf="totalPhotos < 3">
            <div *ngFor="let photo of hotelPhotos | async; index as i;"
                 class="ft-photo ft-photo-{{i + 1}}"
                 [ftBackgroundImage]="photo.Url">
            </div>
        </div>
    `,
    styleUrls: ["./guestPhotos.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class GuestPhotosComponent implements OnInit {

    photos: Observable<Photo[]>;
    totalPhotos: number;
    companyCampaignCode: Store<string>;
    private hotelPhotos: Store<HotelPhoto[]>;

    @Input() maxGuestPhotos: number = 10;

    constructor(public store: Store<AppState>) {
    }

    ngOnInit(): void {
        this.companyCampaignCode = this.store.select(state => state.model.CompanyCampaignCode);
        this.photos = this.store.select(state => state.model.GuestPhotos).map(photos => {
            return photos.filter((photo, index) => {
                return index < this.maxGuestPhotos;
            });
        });
        this.photos.subscribe(photos => {
            this.totalPhotos = photos.length;
        });
        this.hotelPhotos = this.store.select(state => state.model.Property.HotelPhotos);

    }

}