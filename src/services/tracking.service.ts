﻿/**
 * Created by Kevin Tiller on 8/23/2017.
 */

import {Store} from "@ngrx/store";
import {AppState} from "../app.state";
import {Jsonp, URLSearchParams} from "@angular/http";
import {Injectable} from "@angular/core";
import {TimingService} from "./timing.service";

// TODO: THERE BE DRAGONS SIMILAR CODE EXISTS IN confirmation.aspx and buzz.js
@Injectable()
export class TrackingService {
    serviceDomain: string;
    companyCampaignCode: string;
    campaignPromoterUUID: string;
    flipToTracking: any;

    constructor(private jsonp: Jsonp, public store: Store<AppState>) {
        this.flipToTracking = (<any>window)['flipToTracking'] || {};
        this.store.select(state => state.model)
            .subscribe(model => {
                this.companyCampaignCode = model.CompanyCampaignCode;
                this.campaignPromoterUUID = model.Promoter.CampaignPromoterUUID;
                this.serviceDomain = model.ServiceDomain;
            });
    }

    // Send the currently existing tracking data back to the server (happens at LEAST once)
    public sendTrackingData() {
        // Augment what they sent with the company campaign code and campaign promoter uuid
        this.flipToTracking['companyCampaignCode'] = this.companyCampaignCode;
        this.flipToTracking['campaign_promoter_uuid'] = this.campaignPromoterUUID;

        // Send it as a JSONP request to avoid cross-domain problems        
        let params = new URLSearchParams();
        for (let key in this.flipToTracking) {
            params.set(key, this.flipToTracking[key])
        }
        let url = document.location.protocol + '//' + this.serviceDomain + '/confirmation/tracking?' + params.toString();
        this.jsonp.request(url).subscribe(
            // Successful responses call the first callback.
            data => {
            },
            // Errors will call this callback instead:
            err => {
            }
        );
    }


    // Add a tracking data event with an optional time that defaults to current timestamp
    public addTrackingEvent(event: string, time?: number) {
        // If this event has already been set, don't do it again
        if (!this.flipToTracking[event]) {
            this.overrideTrackingEvent(event, time);
        }
    }


    // Add a tracking data event with a required value
    public addTrackingEventValue(event: string, value: string) {
        // Don't do anything if they don't provide a value or it's already set
        if (value && !this.flipToTracking[event]) {
            this.flipToTracking[event] = value;
            this.sendTrackingData();
        }
    }

    // Add a tracking data event with a required value overriding any existing data
    public overrideTrackingEvent(event: string, time?: number) {
        this.flipToTracking[event] = time || TimingService.getTimestamp();
        this.sendTrackingData();
    }
}