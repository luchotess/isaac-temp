/**
 * Created by roman.kupin on 8/8/2017.
 */

import {AppModel, Promotion} from "../app.model";
import {ModelActions} from "../actions/index";
import {SOCIAL_NETWORKS} from "../services/social-networks/socialNetworks";


export type Action = ModelActions.All;

export function modelReducer(state: AppModel, action: Action): AppModel {

    switch (action.type) {
        case ModelActions.SHARED_SUCCESSFULLY: {
            let newPromotion = new Promotion();
            let network = SOCIAL_NETWORKS.find(n => n.name.toLowerCase() === action.network.toLowerCase());
            newPromotion.Title = network.code;
            state.Promoter.Promotions.push(newPromotion);
            return state;
        }
        default: {
            return state;
        }
    }

}
