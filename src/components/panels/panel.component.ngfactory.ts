/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as i0 from '@angular/core';
import * as i1 from '@angular/common';
import * as i2 from './panel.component';
import * as i3 from './panels.service';
const styles_PanelComponent:any[] = ([] as any[]);
export const RenderType_PanelComponent:i0.RendererType2 = i0.ɵcrt({encapsulation:2,
    styles:styles_PanelComponent,data:{}});
function View_PanelComponent_1(_l:any):i0.ɵViewDefinition {
  return i0.ɵvid(0,[i0.ɵncd((null as any),0),(_l()(),i0.ɵand(0,(null as any),(null as any),
      0))],(null as any),(null as any));
}
export function View_PanelComponent_0(_l:any):i0.ɵViewDefinition {
  return i0.ɵvid(0,[(_l()(),i0.ɵted(-1,(null as any),['\n        '])),(_l()(),i0.ɵand(16777216,
      (null as any),(null as any),1,(null as any),View_PanelComponent_1)),i0.ɵdid(2,
      16384,(null as any),0,i1.NgIf,[i0.ViewContainerRef,i0.TemplateRef],{ngIf:[0,
          'ngIf']},(null as any)),(_l()(),i0.ɵted(-1,(null as any),['\n    ']))],(_ck,
      _v) => {
    var _co:i2.PanelComponent = _v.component;
    const currVal_0:any = _co.isActive;
    _ck(_v,2,0,currVal_0);
  },(null as any));
}
export function View_PanelComponent_Host_0(_l:any):i0.ɵViewDefinition {
  return i0.ɵvid(0,[(_l()(),i0.ɵeld(0,0,(null as any),(null as any),1,'ft-panel',([] as any[]),
      (null as any),(null as any),(null as any),View_PanelComponent_0,RenderType_PanelComponent)),
      i0.ɵdid(1,245760,(null as any),0,i2.PanelComponent,[i3.PanelsService],(null as any),
          (null as any))],(_ck,_v) => {
    _ck(_v,1,0);
  },(null as any));
}
export const PanelComponentNgFactory:i0.ComponentFactory<i2.PanelComponent> = i0.ɵccf('ft-panel',
    i2.PanelComponent,View_PanelComponent_Host_0,{id:'id',isActive:'active'},{},['*']);
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiQzovR2l0L1BsYXRmb3JtL0ZsaXBUb1NpdGUvZGlzdC1hcHBzL2NvbmZpcm1hdGlvbi94L2NsaWVudC9zcmMvY29tcG9uZW50cy9wYW5lbHMvcGFuZWwuY29tcG9uZW50Lm5nZmFjdG9yeS50cyIsInZlcnNpb24iOjMsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5nOi8vL0M6L0dpdC9QbGF0Zm9ybS9GbGlwVG9TaXRlL2Rpc3QtYXBwcy9jb25maXJtYXRpb24veC9jbGllbnQvc3JjL2NvbXBvbmVudHMvcGFuZWxzL3BhbmVsLmNvbXBvbmVudC50cyIsIm5nOi8vL0M6L0dpdC9QbGF0Zm9ybS9GbGlwVG9TaXRlL2Rpc3QtYXBwcy9jb25maXJtYXRpb24veC9jbGllbnQvc3JjL2NvbXBvbmVudHMvcGFuZWxzL3BhbmVsLmNvbXBvbmVudC50cy5QYW5lbENvbXBvbmVudC5odG1sIiwibmc6Ly8vQzovR2l0L1BsYXRmb3JtL0ZsaXBUb1NpdGUvZGlzdC1hcHBzL2NvbmZpcm1hdGlvbi94L2NsaWVudC9zcmMvY29tcG9uZW50cy9wYW5lbHMvcGFuZWwuY29tcG9uZW50LnRzLlBhbmVsQ29tcG9uZW50X0hvc3QuaHRtbCJdLCJzb3VyY2VzQ29udGVudCI6WyIgIiwiXG4gICAgICAgIDxuZy1jb250ZW50ICpuZ0lmPVwiaXNBY3RpdmVcIj48L25nLWNvbnRlbnQ+XG4gICAgIiwiPGZ0LXBhbmVsPjwvZnQtcGFuZWw+Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7OzsyQkNDUTs7OztvQkFEUixrREFDUTtNQUFBLDJFQUFBO01BQUE7VUFBQSx3QkFBMEM7OztJQUE5QjtJQUFaLFdBQVksU0FBWjs7OztvQkNEUjtNQUFBO2FBQUE7VUFBQTtJQUFBOzs7OyJ9
