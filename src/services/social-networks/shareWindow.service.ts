/**
 * Created by roman.kupin on 8/15/2017.
 */
import {COMMON_SHARE_WINDOW_FEATURES, SHARE_WINDOW_FEATURES, SocialNetwork} from "./socialNetworks";


export class SocialNetworkShareWindowService {

    isMobile: boolean = false;
    isDebug: boolean = true;

    open(network: SocialNetwork, sourceUrl: string): boolean {
        const features = SocialNetworkShareWindowService.getWindowFeatures(network);
        const aWindow = window.open(sourceUrl, "_blank", features);
        return !!aWindow;
    }


    static getWindowFeatures(network: SocialNetwork) {

        const commonFeatures = COMMON_SHARE_WINDOW_FEATURES;
        const windowFeatures = SHARE_WINDOW_FEATURES.find(value => value.network.code === network.code).features;

        let top = (SocialNetworkShareWindowService.getBrowserHeight() - windowFeatures.height) / 2 + window.screenY;
        let left = (SocialNetworkShareWindowService.getBrowserWidth() - windowFeatures.width) / 2 + window.screenX;

        return `${commonFeatures}width=${windowFeatures.width},height=${windowFeatures.height},top=${top},left=${left}`;
    }


    static getBrowserWidth(): number {
        let x = 0;
        if (window.innerHeight) {
            x = window.innerWidth;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            x = document.documentElement.clientWidth;
        } else if (document.body) {
            x = document.body.clientWidth;
        }
        return x;
    }

    static getBrowserHeight(): number {
        let y = 0;
        if (window.innerHeight) {
            y = window.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            y = document.documentElement.clientHeight;
        } else if (document.body) {
            y = document.body.clientHeight;
        }
        return y;
    }
}
