import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule, JsonpModule, Jsonp, Response} from "@angular/http"
import {Store, StoreModule} from "@ngrx/store";
import {AppComponent} from "./app.component";
import {IntroComponent} from "./views/intro/intro.component";
import {AppState} from "./app.state";
import {AppModel, modelFactory} from "./app.model";
import {MainComponent} from "./views/main/main.component";
import {ReopenerComponent} from "./views/reopener/reopener.component";
import {NO_VIEW} from "./actions/view.actions";
import {viewReducer} from "./reducers/viewReducer";
import {modelReducer} from "./reducers/modelReducer";
import "./polyfills"
import {OverlayComponent, OverlayService} from "./components/overlay/overlay.component";
import {Observable} from "rxjs/Rx";
import * as ViewActions from "./actions/view.actions";
import {PersonNamePipe} from "./pipes/personName.pipe";
import {GuestShareUrlPipe} from "./pipes/guestShareUrl.pipe";
import {PanelsComponent} from "./components/panels/panels.component";
import {PanelComponent} from "./components/panels/panel.component";
import {SetActivePanelDirective} from "./components/panels/setActivePanel.directive";
import {BackgroundImageDirective} from "./components/background-image/backgroundImage.directive";
import {RemoteImageService} from "./services/remoteImage.service";
import {LoopbackService} from "./services/signalrLoopback.service";
import {SocialNetworkShareWindowService} from "./services/social-networks/shareWindow.service";
import {DisabledDirective} from "./components/disabled/disabled.directive";
import {uiReducer} from "./reducers/uiReducer";
import {GuestPhotosComponent} from "./components/guest-photos/guestPhotos.component";
import {CountdownComponent} from "./components/countdown/countdown.component";
import {ClickClassDirective} from "./components/click-class/clickClass.directive";
import {TrackingService} from "./services/tracking.service";
import {PerformanceService} from "./services/performance.service";


declare let model: AppModel;

export function getInitialState(/*state: AppState, action: Action*/): AppState {
    return new AppState(modelFactory(model), NO_VIEW);
}

const reducers = {
    view: viewReducer,
    model: modelReducer,
    UI: uiReducer
};

const components = [
    AppComponent,
    IntroComponent,
    MainComponent,
    ReopenerComponent,
    OverlayComponent,
    PanelsComponent,
    PanelComponent,
    GuestPhotosComponent,
    CountdownComponent
];

const directives = [
    SetActivePanelDirective,
    BackgroundImageDirective,
    DisabledDirective,
    ClickClassDirective
];

const pipes = [
    PersonNamePipe,
    GuestShareUrlPipe
];


@NgModule({
    declarations: [
        ...components,
        ...directives,
        ...pipes
    ],
    entryComponents: [IntroComponent, MainComponent, ReopenerComponent, OverlayComponent],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        HttpModule,
        JsonpModule,
        StoreModule.forRoot(reducers, {initialState: getInitialState})
    ],
    providers: [
        OverlayService,
        RemoteImageService,
        LoopbackService,
        SocialNetworkShareWindowService,
        TrackingService,
        PerformanceService
    ]
})
export class AppModule {
    constructor(private store: Store<AppState>, private performanceService: PerformanceService, private trackingService: TrackingService) {
        store.select(state => state.view).subscribe(view => {
            switch (view) {
                case ViewActions.INTRO_VIEW:
                    performanceService.addPerformanceEvent("ftConfirmationXReady");
                    performanceService.sendPerformanceData();

                    trackingService.addTrackingEvent("confirmationShow");
                    break;

                case ViewActions.MAIN_VIEW:
                    trackingService.addTrackingEvent("viewMainClick");
                    break;
                case ViewActions.REOPENER_VIEW:
                    trackingService.addTrackingEvent("minimizeClick");
                    break;
            }
        });
        Observable.timer(1)
            .subscribe(() => {
                this.store.dispatch(new ViewActions.Intro());
            })
    }
}
