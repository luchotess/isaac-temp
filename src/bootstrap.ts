import {platformBrowser} from "@angular/platform-browser";
import {AppModuleNgFactory} from "./app.module.ngfactory";
import {enableProdMode} from "@angular/core";

enableProdMode();

try { (<any>window)["Typekit"]["load"]({ "async": true });} catch (e) { }
const APP_ROOT = document.createElement("ft-confirmation-app");
document.body.appendChild(APP_ROOT);

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);



