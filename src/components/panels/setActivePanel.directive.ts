/**
 * Created by roman.kupin on 8/14/2017.
 */

import {Directive, HostListener, Input} from "@angular/core";
import {PanelsService} from "./panels.service";



@Directive({
    selector: "[ft-set-active-panel]"
})
export class SetActivePanelDirective {

    @Input("ft-set-active-panel") toBeActivePanelId: string;

    constructor(private panels: PanelsService) {
    }

    @HostListener("click")
    onClick() {
        this.panels.setActiveById(this.toBeActivePanelId);
    }

}