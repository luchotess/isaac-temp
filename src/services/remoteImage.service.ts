/**
 * Created by roman.kupin on 8/15/2017.
 */

import {Observable} from "rxjs/Observable";
import {Subscriber} from "rxjs/Subscriber";

export class RemoteImageService {

    checkExistence(imageUrl: string): Observable<boolean> {
        return new Observable<boolean>(subscriber => {
            if (!imageUrl) {
                subscriber.next(false);
            } else {
                RemoteImageService.checkImageExistenceImpl(imageUrl, subscriber);
            }
        });
    }

    private static checkImageExistenceImpl(imageUrl: string, subscriber: Subscriber<boolean>) {
        const image = new Image();
        image.onload = () => subscriber.next(true);
        image.onerror = () => subscriber.next(false);
        image.src = imageUrl;
    }

}