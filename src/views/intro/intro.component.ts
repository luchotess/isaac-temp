/**
 * Created by roman.kupin on 8/3/2017.
 */


import {Component, ViewEncapsulation, AfterViewInit} from "@angular/core"
import {AppState} from "../../app.state";
import {Store} from "@ngrx/store";
import {Promoter, Property, Timer} from "../../app.model";
import * as ViewActions from "../../actions/view.actions";
import { RemoteImageService } from "../../services/remoteImage.service";
import { TrackingService } from "../../services/tracking.service";

@Component({
    selector: "ft-intro",
    templateUrl: "./intro.component.html",
    styleUrls: ["./intro.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class IntroComponent implements AfterViewInit{

    promoter: Promoter;
    companyCampaignCode: string;
    property: Property;
    timer: Timer;
    isPaused: boolean = false;

    constructor(public store: Store<AppState>,
        public remoteImage: RemoteImageService,
        private trackingService: TrackingService) {

        this.store.select(state => state.model)
            .subscribe(model => {
                this.timer = model.Timer;
                this.promoter = model.Promoter;
                this.property = model.Property;
                this.companyCampaignCode = model.CompanyCampaignCode;
                // preload property bg
                this.remoteImage.checkExistence(this.property.BackgroundPicture);
            });
    }

    ngAfterViewInit() {
        let dataLayer: any = (<any>window)['dataLayer'] || {};
        dataLayer.push({ 'event': 'flipto.optimizeActivate' });
    }

    openMainView() {
        this.store.dispatch(new ViewActions.Main());
    }

    openReopener() {
        this.store.dispatch(new ViewActions.Reopener());
    }

    pause() {
        this.isPaused = true;
    }

    timerTimeout() {
        this.trackingService.addTrackingEvent("timerExpired");
        if (this.timer.ActionType == 0) {
            this.openMainView();
        } else {
            this.openReopener();
        }
    }

}