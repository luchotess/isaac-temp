/**
 * Created by roman.kupin on 8/3/2017.
 */

export interface IPersonName {
    First: string;
    Last: string;
    LastInitial?: string;
}

export class Property {
    UUID: string;
    Logo: string;
    BackgroundPicture: string;
    City: string;
    DisplayName: string;
    Description: string;
    Slug: string;
    TwitterAccount: string;
    PostThumbnailImage: string;
    HotelPhotos: HotelPhoto[];
    GeneralManager: GeneralManager;
}

export function propertyFactory(source: any): Property {
    if (!source) return source;
    let model = new Property();
    model.UUID = source["UUID"];
    model.Logo = source["Logo"];
    model.BackgroundPicture = source["BackgroundPicture"];
    model.City = source["City"];
    model.DisplayName = source["DisplayName"];
    model.Description = source["Description"];
    model.Slug = source["Slug"];
    model.TwitterAccount = source["TwitterAccount"];
    model.PostThumbnailImage = source["PostThumbnailImage"];
    model.HotelPhotos = source["HotelPhotos"] ? source["HotelPhotos"].map(hotelPhotoFactory) : <HotelPhoto[]>[];
    model.GeneralManager = generalManagerFactory(source["GeneralManager"]);
    return model;
}

export class GeneralManager implements IPersonName {
    First: string;
    Last: string;
    ProfileImage: string;
}

export function generalManagerFactory(source: any): GeneralManager {
    if (!source) return source;
    let model = new GeneralManager();
    model.First = source["First"];
    model.Last = source["Last"];
    model.ProfileImage = source["ProfileImage"];
    return model;
}

export class Language {
    Code: string;
    IsRtl: boolean;
}

export function languageFactory(source: any): Language {
    if (!source) return source;
    let model = new Language();
    model.Code = source["Code"];
    model.IsRtl = source["IsRtl"];
    return model;
}

export class Promotion {
    PromotionUUID: string;
    DeliveryUUID: string;
    Title: string;
}

export function promotionFactory(source: any): Promotion {
    if (!source) return source;
    let model = new Promotion();
    model.PromotionUUID = source["PromotionUUID"];
    model.DeliveryUUID = source["DeliveryUUID"];
    model.Title = source["Title"];
    return model;
}

export class Promoter implements IPersonName {
    CampaignPromoterUUID: string;
    PromotionCode: string;
    First: string;
    Last: string;
    Email: string;
    IsHpcSignup: boolean;
    Promotions: Promotion[];
}

export function promoterFactory(source: any): Promoter {
    if (!source) return source;
    let model = new Promoter();
    model.CampaignPromoterUUID = source["CampaignPromoterUUID"];
    model.PromotionCode = source["PromotionCode"];
    model.First = source["First"];
    model.Last = source["Last"];
    model.Email = source["Email"];
    model.IsHpcSignup = source["IsHpcSignup"];
    model.Promotions = source["Promotions"] ? source["Promotions"].map(promotionFactory) : <Promotion[]>[];
    return model;
}

export class Share {
    PostMessage: string;
}

export function shareFactory(source: any): Share {
    if (!source) return source;
    let model = new Share();
    model.PostMessage = source["PostMessage"];
    return model;
}

export class Offer {
    PictureUrl: string;
    FormalText: string;
    CasualText: string
}

export function offerFactory(source: any) {
    if (!source) return source;
    let model = new Offer();
    model.PictureUrl = source["PictureUrl"];
    model.FormalText = source["FormalText"];
    model.CasualText = source["CasualText"];
    return model;
}

export class HotelPhoto {
    AssetImageUUID: string;
    Extension: string;
    Url: string;
}

export function hotelPhotoFactory(source: any): HotelPhoto {
    if (!source) return source;
    let model = new HotelPhoto();
    model.AssetImageUUID = source["AssetImageUUID"];
    model.Extension = source["Extension"];
    model.Url = source["Url"];
    return model;
}

export class Timer {
    Enabled: boolean;
    Duration: number;
    ActionType: number;
}

export function timerFactory(source: any): Timer {
    if (!source) return source;
    let model = new Timer();
    model.Enabled = source["Enabled"];
    model.Duration = source["Duration"];
    model.ActionType = source["ActionType"];
    return model;
}

export class Photo {
    IsMale?: boolean;
    FirstName: string;
    LastInitial: string;
    ProfilePhotoUrl: string;
    SilhouetteClass: string;
    UUID: string;
    PhotoDate: string;
    CreateDate: string;
    Caption: string;
    CaptionInternalRating: number;
    Location: string;
    Score: number;
    CurrentPlace: number;
    TotalVotes: number;
    PhotoWidth: number;
    PhotoHeight: number;
    IsUpdated: boolean;
    LanguageCode: string;
    UserRating: number;
    Quote: string;
    QuoteInternalRating: number;
    IsPositiveRating: boolean;
    InternalRating: number;
    PhotoUriOriginal: string;
    PhotoUriLarge: string;
    PhotoUriSmall: string;
}

export function photoFactory(source: any): Photo {
    if (!source) return source;
    let model = new Photo();
    model.IsMale = source["IsMale"];
    model.FirstName = source["FirstName"];
    model.LastInitial = source["LastInitial"];
    model.ProfilePhotoUrl = source["ProfilePhotoUrl"];
    model.SilhouetteClass = source["SilhouetteClass"];
    model.UUID = source["UUID"];
    model.PhotoDate = source["PhotoDate"];
    model.CreateDate = source["CreateDate"];
    model.Caption = source["Caption"];
    model.CaptionInternalRating = source["CaptionInternalRating"];
    model.Location = source["Location"];
    model.Score = source["Score"];
    model.CurrentPlace = source["CurrentPlace"];
    model.TotalVotes = source["TotalVotes"];
    model.PhotoWidth = source["PhotoWidth"];
    model.PhotoHeight = source["PhotoHeight"];
    model.IsUpdated = source["IsUpdated"];
    model.LanguageCode = source["LanguageCode"];
    model.UserRating = source["UserRating"];
    model.Quote = source["Quote"];
    model.QuoteInternalRating = source["QuoteInternalRating"];
    model.IsPositiveRating = source["IsPositiveRating"];
    model.InternalRating = source["InternalRating"];
    model.PhotoUriOriginal = source["PhotoUriOriginal"];
    model.PhotoUriLarge = source["PhotoUriLarge"];
    model.PhotoUriSmall = source["PhotoUriSmall"];
    return model;
}

export class AppModel {
    CompanyCampaignCode: string;
    CampaignUUID: string;
    CustomCSS: string;
    Property: Property;
    Language: Language;
    Promoter: Promoter;
    Share: Share;
    Offer: Offer[];
    GuestPhotos: Photo[];
    Timer: Timer;
    SignalrUrl: string;
    ServiceDomain: string;
    ScriptParams: any;
}

export function modelFactory(source: any): AppModel {
    if (!source) return source;
    let appModel = new AppModel();
    appModel.CompanyCampaignCode = source["CompanyCampaignCode"];
    appModel.CampaignUUID = source["CampaignUUID"];
    appModel.CustomCSS = source["CustomCSS"];
    appModel.Property = propertyFactory(source["Property"]);
    appModel.Language = languageFactory(source["Language"]);
    appModel.Promoter = promoterFactory(source["Promoter"]);
    appModel.Share = shareFactory(source["Share"]);
    appModel.Offer = source["Offer"] ? source["Offer"].map(offerFactory) : <Offer[]>[];
    appModel.GuestPhotos = source["GuestPhotos"] ? source["GuestPhotos"].map(photoFactory) : <Photo[]>[];
    appModel.Timer = timerFactory(source["Timer"]);
    appModel.SignalrUrl = source["SignalrUrl"];
    appModel.ServiceDomain = source["ServiceDomain"];
    return appModel;
}