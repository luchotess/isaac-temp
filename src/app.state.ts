/**
 * Created by roman.kupin on 8/7/2017.
 */

import {AppModel} from "./app.model";

export class MainViewUI {
    screen: string;

    static SHARE_SCREEN = "share";
    static SHARED_SCREEN = "shared";
}

export class UIState {
    MainView: MainViewUI;
}

export class AppState {

    UI: UIState;

    constructor(public model: AppModel, public view: string) {
        this.UI = new UIState();
    }
}
